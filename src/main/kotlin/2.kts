/*
2. Cargar desde el teclado la pila ORIGEN e inicializar en vacío la pila DESTINO. Pasar todos
los elementos de la pila ORIGEN a la pila DESTINO.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val origen = Stack<String>()
val destino = Stack<String>()

//word used to end data entry
val END_WORD : String = "fin"

//last element entered by the user
var lastElement : String = ""

while(lastElement != END_WORD){
    println("Ingresar elemento o \"$END_WORD\" para finalizar")
    lastElement = readLine()!!
    if(lastElement != END_WORD)
        origen.push(lastElement)
}

while(!origen.empty()){
    destino.push(origen.pop())
}

println("origen: $origen")
println("destino: $destino")