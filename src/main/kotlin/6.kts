/*
6. Pasar el primer elemento (tope) de la pila DADA a su última posición (base), dejando los
restantes elementos en el mismo orden.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<String>()
val aux = Stack<String>()
val aux2 = Stack<String>()

var tope : String = ""

//add some elements to dada
dada.push("uno")
dada.push("dos")
dada.push("tres")
dada.push("cuatro")

//get first element
tope = dada.pop()

while(!dada.empty()){
    aux.push(dada.pop())
}

while(!aux.empty()){
    aux2.push(aux.pop())
}



while(!aux2.empty()){
    dada.push(aux2.pop())
}

dada.push(tope)

println("""
    dada: $dada
    aux1: $aux
    aux2: $aux2
    tope: $tope
""".trimIndent())








