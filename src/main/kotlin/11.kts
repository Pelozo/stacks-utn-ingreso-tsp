/*
11. Suponiendo la existencia de una pila MODELO que no este vacía, eliminar de la pila DADA
todos los elementos que sean iguales al tope de la pila MODELO.
*/

//import stack
import java.util.Stack

//declare and initialize stacks
val modelo = Stack<String>()
val dada = Stack<String>()
val aux = Stack<String>()

var topeModelo : String = ""

//add some elements to stacks
modelo.push("u")
modelo.push("o")
modelo.push("i")
modelo.push("e")
modelo.push("a")

dada.push("a")
dada.push("l")
dada.push("g")
dada.push("o")
dada.push("r")
dada.push("i")
dada.push("t")
dada.push("m")
dada.push("o")

//get modelo first element
topeModelo = modelo.peek()

//loop dada
while(!dada.empty()){
    aux.push(dada.pop())
    if(aux.peek() == topeModelo)
        aux.pop()

}

while(!aux.empty()){
    dada.push(aux.pop())
}


println("dada: $dada")
