/*
8. Repartir los elementos de la pila POZO en las pilas JUGADOR1 y JUGADOR2 en forma
alternativa.
*/

//import stack
import java.util.Stack

//declare and initialize stacks
val pozo = Stack<String>()
val jugador1 = Stack<String>()
val jugador2 = Stack<String>()

var num : Int = 0

//add some elements to pozo
pozo.push("uno")
pozo.push("dos")
pozo.push("tres")
pozo.push("cuatro")
pozo.push("cinco")
pozo.push("seis")

while(!pozo.empty()){
    if(num%2 == 0){
        jugador1.push(pozo.pop())
    }else{
        jugador2.push(pozo.pop())
    }
    num++
}

println("""
    pozo: $pozo
    jugador1: $jugador1
    jugador2: $jugador2

""".trimIndent()
)