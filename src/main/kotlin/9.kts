/*
9. Comparar la cantidad de elementos de las pilas A y B. Mostrar por pantalla el resultado.
 */

//disclaimer: en el enunciado no dice que hay que conservar las pilas

//import stack
import java.util.Stack

//declare and initialize stacks
val a = Stack<String>()
val b = Stack<String>()

//add some elements to stacks

a.push("l")
a.push("o")
a.push("r")
a.push("e")
a.push("m")

b.push("i")
b.push("p")
b.push("s")
b.push("u")
b.push("s")


while(!a.empty() && !b.empty()){
    a.pop()
    b.pop()
}

println("""
    A y B ${if(a.empty() && b.empty()) "tienen la misma cantidad" else "no tienen la misma cantidad"}

""".trimIndent())


