/*
10. Comparar las pilas A y B. Mostrar por pantalla el resultado.
 */

// Medio ambiguo el enunciado.

//import stack
import java.util.Stack

//declare and initialize stacks
val a = Stack<String>()
val b = Stack<String>()

var isIdentical : Boolean = true

//add some elements to stacks
a.push("0118")
a.push("999")
a.push("881")

/*
b.push("0118")
b.push("999")
b.push("881")
*/

b.push("999")
b.push("119")
b.push("725")
b.push("...")
b.push("3")



//compare to see if both stacks are identical
while((!a.isEmpty() && !b.isEmpty()) && isIdentical){
    isIdentical = a.pop().equals(b.pop())

}

println("Las pilas A y B son ${if(isIdentical) "iguales" else "distintas"}")