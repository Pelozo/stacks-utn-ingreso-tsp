/*
5. Cargar desde el teclado la pila DADA. Invertir la pila de manera que DADA contenga los
elementos cargados originalmente en ella, pero en orden inverso.
*/

//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<String>()
val aux = Stack<String>()
val aux2 = Stack<String>()

//word used to end data entry
val END_WORD : String = "fin"

//last element entered by the user
var lastElement : String = ""

while(true){
    println("Ingrese elemento o \"$END_WORD\" para finalizar")
    lastElement = readLine()!!

    if(lastElement == END_WORD)
        break

    dada.push(lastElement)
}

//dada.reverse() :(

while(!dada.isEmpty())
    aux.push(dada.pop())

while(!aux.isEmpty())
    aux2.push(aux.pop())

while(!aux2.isEmpty())
    dada.push(aux2.pop())

println(dada)