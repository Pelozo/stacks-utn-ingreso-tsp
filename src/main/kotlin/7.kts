/*
7. Pasar el último elemento (base) de la pila DADA a su primera posición (tope), dejando los
restantes elementos en el mismo orden.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<String>()
val aux = Stack<String>()

var base : String = ""

//add some elements to dada
dada.push("uno")
dada.push("dos")
dada.push("tres")
dada.push("cuatro")
println("original dada: $dada")


while(!dada.empty()){
    base = dada.pop()
    aux.push(base)
}

//kill the base from aux
aux.pop()

while(!aux.empty()){
    dada.push(aux.pop())
}

//add base to top
dada.push(base)




println("""
    dada: $dada
    aux: $aux
    base: $base

""".trimIndent())










"""
    Inglés del 1er cuatrimestre
    Nivel básico o intermedio.
    examen: lunes 12 a las 14:00
    aula 103
    depende de lo que me saque inglés I o inglés II


    Para inscribirse a cursada nos va a llegar clave SICU desde el depatartamente de alumnos
    por ahora 7 comisiones.

    las que se llena primero es la de 8:30
""".trimIndent()