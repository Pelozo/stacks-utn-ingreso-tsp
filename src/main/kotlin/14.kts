/*
14. Determinar si la cantidad de elementos de la pila DADA es par. Si es par, pasar el
elemento del tope de la pila AUX a la pila PAR y si es impar pasar el tope a la pila IMPAR.
 */

//No tiene mucho sentido el enunciado, o yo no lo entendí.

//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<Int>()
val aux = Stack<Int>()
val par = Stack<Int>()
val impar = Stack<Int>()

var dadaCounter : Int = 0

//add some elements to stacks
dada.push(2)
dada.push(4)
dada.push(8)
dada.push(16)
dada.push(32)

aux.push(64)
aux.push(128)
aux.push(256)

//loop dada
while(!dada.empty()){
    //count element
    dadaCounter++
    dada.pop()
}

if(dadaCounter % 2 == 0){
    par.push(aux.pop())
}else{
    impar.push(aux.pop())
}

println("""
    Cantidad de elementos en dada es: ${if(dadaCounter % 2 == 0) "par" else "impar"}
    par: $par
    impar: $impar
""".trimIndent())