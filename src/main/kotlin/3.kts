/*
3. Cargar desde teclado una pila DADA y pasar a la pila DISTINTOS todos aquellos elementos
distintos al valor 8.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<Int>()
val distintos = Stack<Int>()

//word used to end data entry
val END_WORD : String = "fin"

//last element entered by the user
var lastElement : String = ""

while(lastElement != END_WORD){
    println("Ingrese número o \"$END_WORD\" para finalizar")
    lastElement = readLine()!!

    if(lastElement != END_WORD){
        //if it can be parsed to int add it to dada, otherwise just ignore it.
        lastElement.toIntOrNull()?.let {
            dada.push(it)
            //if different to 8 add to Distintos
            if(it != 8)
                distintos.push(it)
        }
    }

}

println("dada: $dada")
println("distintos: $distintos")