/*

1. Cargar desde el teclado una pila DADA con 5 elementos. Pasar los tres primeros elementos
a la pila AUX1 y los dos restantes a la pila AUX2, ambas pilas inicializadas en vacío.
 */


//import stack
import java.util.Stack

//declare and initialize stacks
val dada = Stack<String>()
val aux1 = Stack<String>()
val aux2 = Stack<String>()

var maxElements : Int = 5


//fill stack
while(maxElements > 0){
    println("Ingresar elemento")
    dada.push(readLine()!!)
    maxElements -= 1
}


for(element in 1..3){
    aux1.push(dada.pop())
}

for(element in 1..2){
    aux2.push(dada.pop())
}

println("dada: $dada")
println("aux1: $aux1")
println("aux2: $aux2")



