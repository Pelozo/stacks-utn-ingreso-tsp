/*
13. Suponiendo la existencia de una pila LÍMITE, pasar los elementos de la pila DADA que
sean mayores o iguales que el tope de LIMITE a la pila MAYORES, y los elementos que sean
menores a la pila MENORES.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val limite = Stack<Int>()
val dada = Stack<Int>()
val mayores = Stack<Int>()
val menores = Stack<Int>()

//add some elements to stacks
limite.push(8)

for(n in 0..12){
    dada.push(n)
}

//loop dada
while(!dada.empty()){

    if(dada.peek() >= limite.peek()){
        mayores.push(dada.pop())
    }else{
        menores.push(dada.pop())
    }

}

println("""
    mayores: $mayores
    menores: $menores


""".trimIndent())