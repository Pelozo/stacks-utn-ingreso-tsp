/*
12. Suponiendo la existencia de una pila MODELO (vacía o no), eliminar de la pila DADA
todos los elementos que existan en MODELO.
 */

//disclaimer: en el enunciado no dice que hay que conservar las pilas



//import stack
import java.util.Stack

//declare and initialize stacks
val modelo = Stack<String>()
val dada = Stack<String>()
val equal = Stack<String>()
val diff = Stack<String>()

//add some elements to stacks
modelo.push("u")
modelo.push("o")
modelo.push("i")
modelo.push("e")
modelo.push("a")

dada.push("a")
dada.push("l")
dada.push("g")
dada.push("o")
dada.push("r")
dada.push("i")
dada.push("t")
dada.push("m")
dada.push("o")


//loop modelo
while(!modelo.empty()) {

    //loop dada
    while(!dada.empty()){

        if(modelo.peek() == dada.peek()){
            equal.push(dada.pop())
        }else{
            diff.push(dada.pop())
        }

    }

    //we put back the differences
    while(!diff.empty()){
        dada.push(diff.pop())
    }

    //remove an element from model
    modelo.pop()

}



println("""

    dada: $dada


""".trimIndent())


