/*
4. Cargar desde el teclado la pila ORIGEN e inicializar en vacío la pila DESTINO. Pasar los
elementos de la pila ORIGEN a la pila DESTINO, pero dejándolos en el mismo orden.
 */

//import stack
import java.util.Stack

//declare and initialize stacks
val origen = Stack<String>()
val destino = Stack<String>()
val aux = Stack<String>()

//word used to end data entry
val END_WORD : String = "fin"

//last element entered by the user
var lastElement : String = ""

while(true){
    println("Ingrese elemento o \"$END_WORD\" para finalizar")
    lastElement = readLine()!!

    if(lastElement == END_WORD)
        break
    origen.push(lastElement)

}

while(!origen.empty()){
    aux.push(origen.pop())
}

while(!aux.empty()){
    destino.push(aux.pop())
}

println("""
    origen: $origen
    destino: $destino
    aux: $aux
""".trimIndent())